import { Component, OnInit } from '@angular/core';
import { UploadServiceService } from '../upload.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  public arqImagem: File;

  public imagem: any

  constructor(private uploadService:UploadServiceService) { }

  ngOnInit() {
  }

  onFileChanged(event: any) {
    this.arqImagem = event.target.files[0];
  }
  onUpload() {
   // this.uploadService.uploadImagem(this.arqImagem);
  }
}
