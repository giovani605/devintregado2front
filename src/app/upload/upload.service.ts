import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UploadServiceService {
  constructor(private http: HttpClient) { }

  public uploadImagem(imgVetor: File,imgMatrix, parametros) {
    var fd = new FormData();
    console.log(parametros);
    fd.append('imgVetor',imgVetor);
    fd.append('imgMatrix',imgMatrix);
    fd.append('dados', JSON.stringify(parametros));
    
    this.http.post("http://localhost:3001/imagem/upload", fd).subscribe(response => {
      console.log(response);
    });
  }
  testar() {
    this.http.get("http://localhost:3001/imagem").subscribe(response => {
      console.log(response);
    });
  }
  buscarTodasImagens(){
    var subs:Subject<any[]> =  new Subject<any[]>();
    this.http.get("http://localhost:3001/imagem").subscribe(response => {
      console.log(response);
      subs.next(response["dados"]);
    });
    return subs.asObservable();
  }
}
