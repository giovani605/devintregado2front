import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { UploadComponent } from './upload/upload/upload.component';
import { HttpClientModule } from '@angular/common/http';
import { PaginaUploadComponent } from './paginas/pagina-upload/pagina-upload.component';
import { Routes, RouterModule } from '@angular/router';
import { PaginaImagensComponent } from './paginas/pagina-imagens/pagina-imagens.component';
import { PaginaInicialComponent } from './paginas/pagina-inicial/pagina-inicial.component';
import { FormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: 'upload', component: PaginaUploadComponent },
  { path: 'imagens', component: PaginaImagensComponent },
  { path: '', component: PaginaInicialComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    PaginaUploadComponent,
    PaginaImagensComponent,
    PaginaInicialComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    NgbModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
