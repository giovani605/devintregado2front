import { Component, OnInit } from '@angular/core';
import { UploadServiceService } from 'src/app/upload/upload.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pagina-imagens',
  templateUrl: './pagina-imagens.component.html',
  styleUrls: ['./pagina-imagens.component.css']
})
export class PaginaImagensComponent implements OnInit {

  public listaImagens: any[] = [];
  constructor(private uploadService: UploadServiceService) { }

  ngOnInit() {
    var subs1: Subscription = this.uploadService.buscarTodasImagens().subscribe(dados => {
      this.listaImagens = dados;
      subs1.unsubscribe();
    })
  }
  getUrlImagem(a){
    console.log("http://localhost:3001/static/"+a);
    return "http://localhost:3001/static/"+a;
  }

}
