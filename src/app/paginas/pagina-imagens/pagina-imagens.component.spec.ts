import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaImagensComponent } from './pagina-imagens.component';

describe('PaginaImagensComponent', () => {
  let component: PaginaImagensComponent;
  let fixture: ComponentFixture<PaginaImagensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaImagensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaImagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
