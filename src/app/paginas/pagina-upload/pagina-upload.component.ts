import { Component, OnInit } from '@angular/core';
import { UploadServiceService } from 'src/app/upload/upload.service';

@Component({
  selector: 'app-pagina-upload',
  templateUrl: './pagina-upload.component.html',
  styleUrls: ['./pagina-upload.component.css']
})
export class PaginaUploadComponent implements OnInit {

  public arqImagemMatrix: File;

  public arqImagemVetor: File;

  public numInteracoes: string;

  public nomeArq:string;

  constructor(private uploadService: UploadServiceService) { }

  ngOnInit() {
  }

  onFileChangedVetor(event: any) {
    this.arqImagemVetor = event.target.files[0];
  }
  onFileChangedMatrix(event: any) {
    this.arqImagemMatrix = event.target.files[0];
  }

  onUpload() {
    console.log(this.numInteracoes);
    this.uploadService.uploadImagem(this.arqImagemVetor, this.arqImagemMatrix, { "interacoes": this.numInteracoes, "nome" : this.nomeArq });
   // this.uploadService.testar();
  }
}
